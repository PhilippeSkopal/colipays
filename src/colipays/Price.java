/**
 * @author Philippe Skopal
 * @version 1.0
 */

package colipays;
import java.util.HashMap;

/**
 * Classe qui calcul le prix de l'affranchissment d'un colis, utiliser dans la classe 'Packet'
 */
public class Price {
	
	private HashMap<String, String> myPrices;
	private HashMap<String, String> myRecommendations;
	private static HashMap<String, String> myLimits;
	
	/**
	 * Constructor, configure le calculateur de prix
	 * @param prices - Map object, key = poids en gramme, en string, value = prix en string
	 * @param recommendations - Map object, key = nom recommend�, value = prix en string
	 * @param limits - Map object, contenant les valeurs maximum pour les colis, key = nom d'une limite en string, value = valeur limite en string
	 */
	public Price(HashMap<String, String> prices, HashMap<String, String> recommendations, HashMap<String, String> limits) {
		
		myPrices = prices;
		myRecommendations = recommendations;
		myLimits = limits;
		        
	}
	
	/**
	 * Retourne le prix en fonction du poids, et du type de recommend�
	 * @param weight - Poids en gramme
	 * @param recommendation - Nom du recommend�
	 * @return Double - retourne le prix pour l'affranchissement du colis en Double
	 */
	public Double getPrice(Integer weight, String recommendation) {
		
		Double result = 0.0;
		
		// add price for the weight
		if(myPrices.containsKey(weight.toString())) {
			result += Double.parseDouble(myPrices.get(weight.toString()));
		}
		
		// if there is a recommendation add to the price
		if(!recommendation.equals("")) {
			if(myRecommendations.containsKey(recommendation)) {
				result += Double.parseDouble(myRecommendations.get(recommendation));
			}
		}
		
		return result;
	}
	
	/**
	 * retourne l'objet map avec les limites entr�es, pour v�rifier la validit� du colis
	 * @return HashMap
	 */
	public HashMap<String, String> getLimits() {
		return myLimits;
	}
	
}
