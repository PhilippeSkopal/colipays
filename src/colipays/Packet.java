/**
 * @author Philippe Skopal
 * @version 1.0
 */

package colipays;
import java.util.HashMap;

/**
 * Classe qui repr�sente un colis. Avec ses dimensions physique. Et le calcul du cout de l'affranchissmeent.
 */
public class Packet {
	
	private Price myPriceCalc = null;
	
	private Double myLength = null; 
	private Double myWidth = null;
	private Double myHeight = null;
	private Integer myWeight = null;
	private String myRecommendationType = null;
	
	private Double myVolume = null;
	private Double myDensity = null;
	private Boolean myIsValid = null;
	private Double myStampCost = null;
	
	public Double geLength() {return myLength;}
	public Double getWidth() {return myWidth;}
	public Double getHeight() {return myHeight;}
	public Integer getWeight() {return myWeight;}
	public String getRecommendationType() {return myRecommendationType;}
	
	public Double getVolume() {return myVolume;}
	public Double getDensity() {return myDensity;}
	public Boolean getIsValid() {return myIsValid;}
	public Double getStampCost() {return myStampCost;}
	
	/**
	 * Contructor
	 * @param length - Longueur en m�tre
	 * @param width - Largeur en m�tre
	 * @param height - Hauteur  en m�tre
	 * @param weight - Poids en gramme
	 * @param recommendationType - Type de recommendation
	 */
	public Packet(Double length, Double width, Double height, Integer weight, String recommendationType) {
		
		myPriceCalc = getPriceConfigured();
		
		myLength = length;
		myWidth = width;
		myHeight = height;
		myWeight = weight;
		myRecommendationType = recommendationType;
		
		doCalc();
		
	}
	
	/**
	 * 
	 * @return <Price> - Retourne un objet qui calcul le prix de l'affranchissement de colis
	 */
	private Price getPriceConfigured() {
		
		HashMap<String, String> myPrices = new HashMap<String, String>();
		HashMap<String, String> myRecommendations = new HashMap<String, String>();
		HashMap<String, String> myLimits = new HashMap<String, String>();
		
		myPrices.put("250", "5.50");
		myPrices.put("500", "6.10");
		myPrices.put("750", "6.90");
		myPrices.put("1000", "7.50");
		myPrices.put("2000", "8.50");
		myPrices.put("3000", "9.70");
		myPrices.put("5000", "11.90");
		myPrices.put("7000", "14.10");
		myPrices.put("10000", "17.40");
		myPrices.put("15000", "20.15");
		myPrices.put("20000", "28.35");

		myRecommendations.put("R1", "2.50");
		myRecommendations.put("R2", "3.40");
		myRecommendations.put("R3", "4.60");
		myRecommendations.put("R4", "5.80");
		myRecommendations.put("R5", "7.00");
		
		myLimits.put("longueurLargeurHauteur", "1.5");
		myLimits.put("longueur", "1.0");
		
		return new Price(myPrices, myRecommendations, myLimits);
		
	}
	
	/**
	 * Effectue le calcul du prix der l'affranchissment du calcul, s'ex�cute dans le constructeur
	 */
	private void doCalc() {
		
		myVolume = myLength * myWidth * myHeight;
		myDensity = (myWeight /1000) / myVolume;
		
		HashMap<String, String> myLimits = myPriceCalc.getLimits();
		if(myLimits.containsKey("longueurLargeurHauteur") && myLimits.containsKey("longueur")) {
			
			Double longueurLargeurHauteur = Double.parseDouble(myLimits.get("longueurLargeurHauteur"));
			Double longueur = Double.parseDouble(myLimits.get("longueur"));
			
			if(myLength + myWidth + myHeight <= longueurLargeurHauteur && longueur >= myLength) {
				myIsValid = true;
			}else {
				myIsValid = false;
			}
		}else {
			myIsValid = false;
		}
		
		myStampCost = myPriceCalc.getPrice(myWeight, myRecommendationType);
		
	}
	
}
