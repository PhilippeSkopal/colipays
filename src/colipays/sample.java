/**
 * @author Philippe Skopal
 * @version 1.0
 * Simple sample file, speaks by it-self
 */

package colipays;
import java.util.HashMap;

public class sample {

	public static void main(String[] args) {
		
		// #### Un-comment any to test ####
		
		// sample_serializer();
		// sample_price();
		sample_packet();
		
	}
	
	public static void sample_serializer() {
		
		Serializer test = new Serializer();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("test", "test");
		
		// serialize
		test.serialize(map, "test.ser");
		
		// deserialize
		HashMap<String, String> newMap = test.deserialize("test.ser");
		
		System.out.println(map.toString());
		System.out.println(newMap.toString());
		
	}
	
	public static void sample_price() {
		
		HashMap<String, String> myPrices = new HashMap<String, String>();
		HashMap<String, String> myRecommendations = new HashMap<String, String>();
		HashMap<String, String> myLimits = new HashMap<String, String>();
		
		myPrices.put("250", "5.50");
		myPrices.put("500", "6.10");
		myPrices.put("750", "6.90");
		myPrices.put("1000", "7.50");
		myPrices.put("2000", "8.50");
		myPrices.put("3000", "9.70");
		myPrices.put("5000", "11.90");
		myPrices.put("7000", "14.10");
		myPrices.put("10000", "17.40");
		myPrices.put("15000", "20.15");
		myPrices.put("20000", "28.35");

		myRecommendations.put("R1", "2.50");
		myRecommendations.put("R2", "3.40");
		myRecommendations.put("R3", "4.60");
		myRecommendations.put("R4", "5.80");
		myRecommendations.put("R5", "7.00");
		
		myLimits.put("longueurLargeurHauteur", "1.5");
		myLimits.put("longueur", "1.0");
		
		Price price = new Price(myPrices, myRecommendations, myLimits);
		
		// ==> 11.90 + 4.60 = 16.50
		Double result = price.getPrice(5000, "R3");
		System.out.println(result);
		
		// 28.35
		result = price.getPrice(20000, "");
		System.out.println(result);
		
	}
	
	public static void sample_packet() {
			
		// (Double length, Double width, Double height, Integer weight, String recommendationType)
		
		Packet test_ok1 = new Packet(1.0, 0.2, 0.2, 7000,"R4");
		
		System.out.println(test_ok1.getDensity());
		System.out.println(test_ok1.getVolume());
		System.out.println(test_ok1.getIsValid());
		System.out.println(test_ok1.getStampCost());
		
		/*Packet test_ok2 = new Packet(1.0, 1.0, 1.0, 7000,"R4");
		
		System.out.println(test_ok1.getIsValid());
		System.out.println(test_ok2.getIsValid());
		
		System.out.println(test_ok1.getStampCost());
		System.out.println(test_ok1.getStampCost());
		
		Packet test_ko = new Packet(2.0, 2.0, 2.0, 7000,"R4");
		
		System.out.println(test_ko.getIsValid());
		System.out.println(test_ko.getStampCost());*/
		
		
	}

}
