package colipays;

import java.io.*;
import java.util.HashMap;

public class Serializer {
	
	public void serialize(HashMap<String, String> obj, String path) {
		
		try{
			FileOutputStream fos = new FileOutputStream(path);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
			oos.close();
			fos.close();
		}catch(IOException ioe){
        	ioe.printStackTrace();
		}
	}
	
	public HashMap<String, String> deserialize(String path){
		
		HashMap<String, String> map = null;
		try{
			FileInputStream fis = new FileInputStream(path);
			ObjectInputStream ois = new ObjectInputStream(fis);
			map = (HashMap) ois.readObject();
			ois.close();
			fis.close();
		}catch(IOException ioe){	
			ioe.printStackTrace();
			return new HashMap<String, String>();
		}catch(ClassNotFoundException c){
			System.out.println("Class not found");
			c.printStackTrace();
			return new HashMap<String, String>();
		}
		
		return map;
	}
	
}
